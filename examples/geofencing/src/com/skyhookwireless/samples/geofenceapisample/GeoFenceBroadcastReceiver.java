package com.skyhookwireless.samples.geofenceapisample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GeoFenceBroadcastReceiver
    extends BroadcastReceiver
{
    public GeoFenceBroadcastReceiver()
    {}

    @Override
    public void onReceive(Context context, Intent intent)
    {
        final Intent launchIntent =
                new Intent(intent.getAction(), null, context, GeoFenceSampleActivity.class);

        launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (intent.getExtras() != null)
            launchIntent.putExtras(intent.getExtras());

        context.startActivity(launchIntent);
    }
}
