package com.skyhookwireless.samples.geofenceapisample;

import com.mapquest.android.maps.GeoPoint;
import com.mapquest.android.maps.MapActivity;
import com.mapquest.android.maps.MapView;
import com.skyhookwireless.samples.geofenceapisample.R;
import com.skyhookwireless.wps.WPSGeoFence;
import com.skyhookwireless.wps.WPSLocation;
import com.skyhookwireless.wps.WPSReturnCode;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Vibrator;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class GeoFenceSampleActivity
    extends MapActivity
{
    private static final String TAG = "GeoFenceSampleActivity";
    private MapView _mapView;
    private LocationOverlay _locationOverlay;
    private LocationOverlay _geoFenceOverlay;

    private BroadcastReceiver _locationUpdateReceiver;

    private boolean _waitingForLocation;

    private WPSLocation _lastLocation;
    private WPSGeoFence _geoFence;

    public final static int DEFAULT_GEOFENCE_HPE_COLOR = Color.argb(100, 0, 173, 132);
    public final static int TRIGGERED_GEOFENCE_COLOR = Color.argb(100, 255, 101, 117);

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if (_geoFence != null)
            outState.putSerializable(LocationService.XPS_GEOFENCE_EXTRA, _geoFence);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_fence_sample);
        _mapView = (MapView)findViewById(R.id.mapView);

        registerForLocationUpdateIntents();
        requestLocationUpdate();
        setupOnScreenButtons();

        if (savedInstanceState != null)
            restoreGeoFence(savedInstanceState);

        // If this activity was started because we crossed over a geofence
        // boundary we need to get the intent and update the screen accordingly
        handleGeoFenceIntent(getIntent());

        // Put the map at a reasonable zoom level to start
        if (_mapView.getZoomLevel() < 15)
            _mapView.getController().setZoom(15);
    }

    private void registerForLocationUpdateIntents()
    {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(LocationService.LOCATION_UPDATE_INTENT);

        _locationUpdateReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(final Context context, final Intent intent)
            {
                if (intent.getAction().equals(LocationService.LOCATION_UPDATE_INTENT))
                {
                    WPSReturnCode rc =
                            (WPSReturnCode) intent.getSerializableExtra(LocationService.XPS_STATUS_EXTRA);

                    if (rc != WPSReturnCode.WPS_OK)
                    {
                        Log.d(TAG, "Received location update error: " + rc);
                        return;
                    }

                    WPSLocation location =
                            (WPSLocation) intent.getSerializableExtra(LocationService.XPS_LOCATION_EXTRA);

                    Log.d(TAG, "Receieved location update: " + location);
                    onLocation(location);
                }
            }
        };

        registerReceiver(_locationUpdateReceiver, filter);
    }

    private void setupOnScreenButtons()
    {
        final Button gotoFenceButton = (Button)findViewById(R.id.gotogeofence);
        gotoFenceButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (_geoFence != null)
                {
                    final GeoPoint point = toGeoPoint(_geoFence.getLatitude(),
                                                      _geoFence.getLongitude());
                    _mapView.getController().animateTo(point);
                }
            }
        });

        final Button gotoMyLocation = (Button)findViewById(R.id.gotomylocation);
        gotoMyLocation.setOnClickListener(new OnClickListener()
        {
            private long _lastClick = -1;

            @Override
            public void onClick(View v)
            {
                // Prevent the UI from generating too many requests.
                final long current = SystemClock.elapsedRealtime();
                if (_lastClick == -1
                        || current - _lastClick > 3000
                        || current < _lastClick)
                {
                    _lastClick = current;
                    onClickAux(v);
                }
            }

            private void onClickAux(View v)
            {
                if (isNewLocationNeeded())
                    requestLocationUpdate();
            }
        });

        final Button updateMyLocation = (Button)findViewById(R.id.updatemylocation);
        updateMyLocation.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (_lastLocation != null)
                    _mapView.getController().animateTo(toGeoPoint(_lastLocation));
            }
        });

        final Button clearGeoFence = (Button)findViewById(R.id.cleargeofence);
        clearGeoFence.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                removeGeoFence();
            }
        });

        final Button setGeoFence = (Button)findViewById(R.id.setgeofence);
        setGeoFence.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                _handler.sendMessage(_handler.obtainMessage(GEOFENCE_MESSAGE,
                                                            _mapView.getMapCenter()));
            }
        });
    }

    private boolean isNewLocationNeeded()
    {
        // Only request a location update if the last location is at least 3 seconds old.
        return _lastLocation == null
            || System.currentTimeMillis() - _lastLocation.getTime() > 3000;
    }

    private void requestLocationUpdate()
    {
        _waitingForLocation = true;
        LocationService.requestLocationUpdate(getApplicationContext());
    }

    private void restoreGeoFence(Bundle saved)
    {
        WPSGeoFence fence = (WPSGeoFence) saved.getSerializable(LocationService.XPS_GEOFENCE_EXTRA);
        if (fence != null)
            addGeoFence(toGeoPoint(fence.getLatitude(), fence.getLongitude()),
                        fence.getRadius(),
                        fence.getType(),
                        fence.getPeriod());
    }

    @Override
    protected void onDestroy()
    {
        if (_locationUpdateReceiver != null)
            unregisterReceiver(_locationUpdateReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_geo_fence_sample, menu);
        return true;
    }

    private final static String HELP_TEXT =
            "Position the crosshairs at the location to create the new geofence. Press the Set "
           +"Geofence button to create the geofence at that location. Only one geofence "
           +"is configured at a time. Creating a new geofence replaces an existing one.\n\n"
           +"The geofence turns red and the device vibrates when the geofence boundary triggered";

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item)
    {
        // Only option is help
        AlertDialog help =
                new AlertDialog.Builder(GeoFenceSampleActivity.this)
                    .setTitle("Help")
                    .setMessage(HELP_TEXT)
                    .setPositiveButton("OK", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {}
                    })
                    .create();
        help.show();
        return super.onMenuItemSelected(featureId, item);
    }

    @Override
    protected boolean isRouteDisplayed()
    {
        return false;
    }

    private void addGeoFence(final GeoPoint location, final int radius, WPSGeoFence.Type type, final long period)
    {
        WPSGeoFence newGeoFence = new WPSGeoFence(location.getLatitudeE6() / 1E6,
                                                  location.getLongitudeE6() / 1E6,
                                                  radius,
                                                  type,
                                                  period);

        LocationService.clearGeoFences(getApplicationContext());
        LocationService.setGeofence(getApplicationContext(), newGeoFence);

        // NOTE: The sample app only keeps one geofence at a time to keep things simple.
        //       Remove the previous geofence.
        _geoFence = newGeoFence;
        putGeoFenceOnMap(location, radius);
        _geoFenceOverlay.setHpeCircleColor(DEFAULT_GEOFENCE_HPE_COLOR);
    }

    private void removeGeoFence()
    {
        if (_geoFence != null)
        {
            LocationService.clearGeoFences(getApplicationContext());
            _geoFence = null;
            _mapView.getOverlays().removeAll(_geoFenceOverlay.getOverlays());
            _geoFenceOverlay = null;
            _mapView.invalidate();
        }
    }

    private void handleGeoFenceIntent(final Intent intent)
    {
        final String action = intent.getAction();
        if (LocationService.GEOFENCE_TRIGGERED_INTENT.equals(action))
        {
            final WPSGeoFence geoFence = (WPSGeoFence)intent.getSerializableExtra(LocationService.XPS_GEOFENCE_EXTRA);
            final WPSLocation location = (WPSLocation)intent.getSerializableExtra(LocationService.XPS_LOCATION_EXTRA);

            if (geoFence == null || location == null)
                return;

            Log.d(TAG, "You have triggered geofence: "+ geoFence +" at "+location);

            final String message =
                    (geoFence.getType().equals(WPSGeoFence.Type.WPS_GEOFENCE_ENTER)
                  || geoFence.getType().equals(WPSGeoFence.Type.WPS_GEOFENCE_INSIDE))
                        ? "You have entered the Geofence!"
                        : "You have left the Geofence!";

            Toast.makeText(GeoFenceSampleActivity.this,
                           message,
                           Toast.LENGTH_LONG).show();

            putGeoFenceOnMap(toGeoPoint(geoFence.getLatitude(), geoFence.getLongitude()), geoFence.getRadius());
            _geoFenceOverlay.setHpeCircleColor(TRIGGERED_GEOFENCE_COLOR);
            onLocation(location);

            // Vibrate the phone when the user crosses the geofence
            Vibrator v = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(1000);
        }
    }

    @Override
    public void onNewIntent(Intent intent)
    {
        handleGeoFenceIntent(intent);
        super.onNewIntent(intent);
    }

    private void onLocation(final WPSLocation location)
    {
        _lastLocation = location;
        _handler.sendMessage(_handler.obtainMessage(LOCATION_MESSAGE,
                             location));
    }

    public void done()
    {}

    private static final int LOCATION_MESSAGE = 1;
    private static final int GEOFENCE_MESSAGE = 2;

    private final static CharSequence[] typeStrings = {"ENTER", "LEAVE", "INSIDE", "OUTSIDE"};
    private final static WPSGeoFence.Type[] fenceTypes = {WPSGeoFence.Type.WPS_GEOFENCE_ENTER,
                                                          WPSGeoFence.Type.WPS_GEOFENCE_LEAVE,
                                                          WPSGeoFence.Type.WPS_GEOFENCE_INSIDE,
                                                          WPSGeoFence.Type.WPS_GEOFENCE_OUTSIDE};

    private final Handler _handler = new Handler()
    {
        public void handleMessage(final Message msg)
        {
            if (msg.what == LOCATION_MESSAGE)
            {
                WPSLocation loc = (WPSLocation) msg.obj;
                GeoPoint point = toGeoPoint(loc);
                if (_waitingForLocation)
                {
                    _mapView.getController().animateTo(point);
                    _waitingForLocation = false;
                }
                putLocationOnMap(point, loc.getHPE());
            }
            else if (msg.what == GEOFENCE_MESSAGE)
            {
                LayoutInflater factory = LayoutInflater.from(GeoFenceSampleActivity.this);
                final View textEntryView = factory.inflate(R.layout.new_geofence_dialog_text_entry, null);

                final Spinner spinner = (Spinner) textEntryView.findViewById(R.id.typeSpinner);
                ArrayAdapter<CharSequence> adapter =
                        new ArrayAdapter<CharSequence>(GeoFenceSampleActivity.this,
                                                       android.R.layout.simple_spinner_item,
                                                       typeStrings);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                AlertDialog createGeoFence =  new AlertDialog.Builder(GeoFenceSampleActivity.this)
                    .setTitle("Create New Geofence")
                    .setView(textEntryView)
                    .setPositiveButton("Set", new AlertDialog.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int whichButton)
                        {
                            final AlertDialog alertDialog = (AlertDialog) dialog;
                            final String radiusString =
                                    ((EditText)alertDialog.findViewById(R.id.geofence_radius)).getText().toString();
                            final int position = spinner.getSelectedItemPosition();
                            final String periodString =
                                    ((EditText)alertDialog.findViewById(R.id.geofence_period)).getText().toString();

                            try
                            {
                                final int radius = Integer.valueOf(radiusString);
                                final long period = Long.valueOf(periodString) * 1000; // Convert to ms
                                addGeoFence(point,
                                            radius,
                                            fenceTypes[position],
                                            period);
                            }
                            catch (NumberFormatException e)
                            {
                                Toast.makeText(GeoFenceSampleActivity.this, "Invalid Geofence", Toast.LENGTH_SHORT).show();
                            }
                        }
                        final GeoPoint point = (GeoPoint)msg.obj;
                    })
                    .create();
                createGeoFence.show();
            }
        }
    };

    private void putGeoFenceOnMap(final GeoPoint location, final int radius)
    {
        if (_geoFenceOverlay == null)
        {
            _geoFenceOverlay = new LocationOverlay(location, radius);
            _mapView.getOverlays().addAll(_geoFenceOverlay.getOverlays());
        }
        else
        {
            _geoFenceOverlay.updateLocation(location, radius);
        }
        _mapView.invalidate();
    }

    private void putLocationOnMap(final GeoPoint location, final int radius)
    {
        if (_locationOverlay == null)
        {
            _locationOverlay = new LocationOverlay(location, radius);
            _mapView.getOverlays().addAll(_locationOverlay.getOverlays());
        }
        else
        {
            _locationOverlay.updateLocation(location, radius);
        }
        _mapView.invalidate();
    }

    private static GeoPoint toGeoPoint(WPSLocation location)
    {
        return toGeoPoint(location.getLatitude(), location.getLongitude());
    }

    private static GeoPoint toGeoPoint(double lat, double lon)
    {
        lat = Math.round(lat * 1E6) / 1E6;
        lon = Math.round(lon * 1E6) / 1E6;
        return new GeoPoint((int)(lat * 1E6),
                            (int)(lon * 1E6));
    }
}
