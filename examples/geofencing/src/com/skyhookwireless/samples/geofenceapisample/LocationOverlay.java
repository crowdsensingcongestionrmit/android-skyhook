package com.skyhookwireless.samples.geofenceapisample;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import com.mapquest.android.maps.CircleOverlay;
import com.mapquest.android.maps.GeoPoint;
import com.mapquest.android.maps.Overlay;

public class LocationOverlay
{
    public final static int DEFAULT_HPE_COLOR = Color.argb(100, 153, 204, 255);

    private final CircleOverlay _center;
    private final CircleOverlay _hpe;
    private final List<Overlay> _overlays;
    private int _hpeCircle = DEFAULT_HPE_COLOR;

    public LocationOverlay(GeoPoint location, final int hpe)
    {
        Paint paint = new Paint(Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.FILL);
        paint.setARGB(100, 36, 36, 198);
        paint.setStrokeWidth(2);
        _center = new CircleOverlay(location, 6, paint);

        paint = new Paint(Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.FILL);;
        paint.setColor(_hpeCircle);
        _hpe = new CircleOverlay(location, (double)hpe, paint);

        _overlays = new ArrayList<Overlay>(2);
        _overlays.add(_hpe);
        _overlays.add(_center);
    }

    public List<Overlay> getOverlays()
    {
        return _overlays;
    }

    public void updateLocation(GeoPoint location, final int hpe)
    {
        _center.setCenter(location);
        _hpe.setCenter(location);
        _hpe.setRadiusMeters((double) hpe);
    }

    public void setHpeCircleColor(final int color)
    {
        Paint paint = new Paint(Paint.SUBPIXEL_TEXT_FLAG 
                              | Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Style.FILL);
        paint.setColor(color);
        _hpe.setPaint(paint);
    }
}
