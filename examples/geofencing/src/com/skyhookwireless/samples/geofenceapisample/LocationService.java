package com.skyhookwireless.samples.geofenceapisample;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.skyhookwireless.wps.GeoFenceCallback;
import com.skyhookwireless.wps.TilingListener;
import com.skyhookwireless.wps.WPSAuthentication;
import com.skyhookwireless.wps.WPSContinuation;
import com.skyhookwireless.wps.WPSGeoFence;
import com.skyhookwireless.wps.WPSLocation;
import com.skyhookwireless.wps.WPSPeriodicLocationCallback;
import com.skyhookwireless.wps.WPSReturnCode;
import com.skyhookwireless.wps.WPSStreetAddressLookup;
import com.skyhookwireless.wps.XPS;
import com.skyhookwireless.wps.WPSGeoFence.Handle;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class LocationService
    extends Service
{
    /**
     *  Broadcast intent action requesting a single location update, LOCATION_UPDATE_INTENT will be broadcast in response
     *  @see #LOCATION_UPDATE_INTENT
     */
    public final static String GET_LOCATION_UPDATE_INTENT = "com.skyhookwireless.samples.geofenceapisample.GET_LOCATION_UPDATE_INTENT";

    /**
     *  Broadcast intent action request to cancel all geofences
     */
    public final static String CLEAR_GEOFENCES_INTENT = "com.skyhookwireless.samples.geofenceapisample.CLEAR_GEOFENCES_INTENT";

    /**
     *  Broadcast intent action request to cancel all geofences
     *  Must include:
     *    @see #XPS_GEOFENCE_EXTRA
     */
    public final static String SET_GEOFENCE_INTENT = "com.skyhookwireless.samples.geofenceapisample.SET_GEOFENCE_INTENT";

    /**
     * Broadcast intent action indicating the user has crossed a geofence boundary.
     * Includes:
     *     @see #XPS_GEOFENCE_EXTRA
     *     @see #XPS_LOCATION_EXTRA
     */
    public final static String GEOFENCE_TRIGGERED_INTENT = "com.skyhookwireless.samples.geofenceapisample.GEOFENCE_TRIGGERED_INTENT";

    /**
     * Broadcast intent including XPS location update
     * Includes:
     *     @see #XPS_STATUS_EXTRA
     *     @see #XPS_LOCATION_EXTRA if WPS_OK
     */
    public final static String LOCATION_UPDATE_INTENT = "com.skyhookwireless.samples.geofenceapisample.LOCATION_UPDATE_INTENT";

    // Extras
    public final static String XPS_STATUS_EXTRA          = "XPS_STATUS_EXTRA";
    public final static String XPS_GEOFENCE_EXTRA        = "XPS_GEOFENCE_EXTRA";
    public final static String XPS_LOCATION_EXTRA        = "XPS_LOCATION_EXTRA";

    private final static String TAG = "LocationService";

    private XPS _xps;

    // List of all active geofence handles
    private List<Handle> _fences;

    private volatile boolean _isLocationRequestPending = false;

    /*
     * Convenience methods
     */

    public static void clearGeoFences(final Context context)
    {
        context.startService(new Intent(CLEAR_GEOFENCES_INTENT, null, context, LocationService.class));
    }

    public static void requestLocationUpdate(final Context context)
    {
        context.startService(new Intent(GET_LOCATION_UPDATE_INTENT, null, context, LocationService.class));
    }

    public static void setGeofence(final Context context, WPSGeoFence newFence)
    {
        final Intent intent = new Intent(SET_GEOFENCE_INTENT, null, context, LocationService.class);
        intent.putExtra(XPS_GEOFENCE_EXTRA, newFence);
        context.startService(intent);
    }

    public LocationService() {}

    // Username and Realm information obtained from Skyhook
    private static final String WPS_USERNAME = SkyhookConstants.getUsername();
    private static final String WPS_REALM = SkyhookConstants.getRealm();

    private final static WPSAuthentication _auth
            = new WPSAuthentication(WPS_USERNAME, WPS_REALM);

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Create the XPS object
        _xps = new XPS(getApplicationContext());

        // Set your registration user
        _xps.setRegistrationUser(_auth);

        // Enable tiling
        configureTiling();

        // Keep track of the configured geofences
        _fences = new ArrayList<Handle>();
        _isLocationRequestPending = false;
    }

    private void configureTiling()
    {
        // Determine where your application will download the tiles.
        // For example, Android provides a location for application to
        // store files on  external storage. The getExternalFilesDir() method
        // requires the WRITE_EXTERNAL_STORAGE permission.
        final File pathToStoreTilesFile = getExternalFilesDir(null);
        final String pathToStoreTiles = pathToStoreTilesFile.getAbsolutePath();

        // Setup tiling so location determination can be performed locally.
        // Tiles are typically less then 50KB in size, so to download an area of 3x3
        // tiles for each session you would set maxDataSizePerSession to 450KB,
        // i.e. 460,800. It is recommended that maxDataSizePerSession be a factor
        // of 2 - 10 smaller than maxDataSizeTotal, so that tiles from several areas
        // can be cached
        int maxDataSizePerSession = 200 * 1024; // 200KB
        int maxDataSizeTotal = 400 * 1024;      // 400KB

        final TilingListener listener = new TilingListener()
        {
            @Override
            public WPSContinuation tilingCallback(int tileNumber, int tileTotal)
            {
                Log.d(TAG,
                      "tilingCallback with tileNumber "+tileNumber+
                      " of "+tileTotal+"total tiles");

                // Applications can control the number of tiles to download based
                // on the current network and power state of the device.
                // For example, only download 4 tiles when new tiles are required.
                return (tileNumber < 4)
                           ? WPSContinuation.WPS_CONTINUE
                           : WPSContinuation.WPS_STOP;
            }
        };

        _xps.setTiling(pathToStoreTiles,
                       maxDataSizePerSession,
                       maxDataSizeTotal,
                       listener);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent == null)
            return START_STICKY;

        // Handle any intent request actions defined by our service's API.
        final String action = intent.getAction();
        if (SET_GEOFENCE_INTENT.equals(action))
        {
            final WPSGeoFence fence =
                    (WPSGeoFence) intent.getExtras().getSerializable(XPS_GEOFENCE_EXTRA);

            if (fence != null)
                setGeoFence(fence);
        }
        else if (CLEAR_GEOFENCES_INTENT.equals(action))
        {
            _fences.clear();
            _xps.cancelAllGeoFences();
        }
        else if (GET_LOCATION_UPDATE_INTENT.equals(action))
        {
            _isLocationRequestPending = true;

            // We will force a new location update by calling getPeriodicLocation() again.
            //
            // IMPORTANT: We will continue to use the geofencing period of 1 hour because
            //            we only need to update the location once. By using the same callback
            //            we do not interrupt location updates for geofencing.
            getLocatioUpdates();
        }

        return START_STICKY;
    }

    private void setGeoFence(WPSGeoFence newFence)
    {
        // Define the callback that will be invoked when the user crosses the geofence boundary.
        GeoFenceCallback callback = new GeoFenceCallback()
        {
            @Override
            public WPSContinuation handleGeoFence(WPSGeoFence geofence, WPSLocation location)
            {
                Log.d(TAG, "GeoFence "+ geofence +"was triggered at "+location);

                // In our example we are sending an intent to notify our application that
                // the user has crossed the geofence boundary.
                Intent intent = new Intent(GEOFENCE_TRIGGERED_INTENT, null);
                intent.putExtra(XPS_GEOFENCE_EXTRA, geofence);
                intent.putExtra(XPS_LOCATION_EXTRA, location);
                sendBroadcast(intent);

                // We are returning WPS_CONTINUE because we want to be continue
                // being notified when the user crosses over the geofence boundary.
                //
                // If you do not want to continue receiving updates return WPS_STOP.
                return WPSContinuation.WPS_CONTINUE;
            }
        };

        final Handle handle = _xps.setGeoFence(newFence, callback);

        // GeoFences will only be monitored in the context of pending periodic location requests
        //
        // Make sure that we are requesting location updates while geofences are configured.
        if (handle != null)
        {
            _fences.add(handle);

            // Begin monitoring the geofences by enabling location updates.
            getLocatioUpdates();
        }
    }

    private void getLocatioUpdates()
    {
        // IMPORTANT: Geofences are monitored in the context of ongoing location updates.
        //
        //            XPS will override the location period below and determine the optimal
        //            period based on the configured geofences.
        //
        //            When location updates are only required for Geofencing we'll use
        //            a long period: 1 hour.
        final int period = 60 * 60 * 1000;

        // A value of zero indicates an unlimited number of iterations.
        final int iterations = 0;

        Log.d(TAG, "Starting location with period of "+period+" seconds");
        _xps.getPeriodicLocation(null,
                                 WPSStreetAddressLookup.WPS_NO_STREET_ADDRESS_LOOKUP,
                                 period,
                                 iterations,
                                 _callback);
    }

    private final WPSPeriodicLocationCallback _callback = new WPSPeriodicLocationCallback()
    {
        @Override
        public void done()
        {
            Log.d(TAG, "periodic location done()");
        }

        @Override
        public WPSContinuation handleError(WPSReturnCode error)
        {
            Log.d(TAG, "handleError: "+error);

            sendLocationErrorIntent(error);

            return _isLocationRequestPending
                       || ! _fences.isEmpty() ? WPSContinuation.WPS_CONTINUE
                                              : WPSContinuation.WPS_STOP;
        }

        @Override
        public WPSContinuation handleWPSPeriodicLocation(WPSLocation location)
        {
            Log.d(TAG, "handleWPSPeriodicLocation: "+location);

            _isLocationRequestPending = false;
            sendLocationIntent(location);

            return ! _fences.isEmpty() ? WPSContinuation.WPS_CONTINUE
                                       : WPSContinuation.WPS_STOP;
        }
    };

    private void sendLocationIntent(final WPSLocation location)
    {
        final Intent intent = new Intent(LOCATION_UPDATE_INTENT, null);
        intent.putExtra(XPS_STATUS_EXTRA, WPSReturnCode.WPS_OK);
        intent.putExtra(XPS_LOCATION_EXTRA, location);
        sendBroadcast(intent);
    }

    private void sendLocationErrorIntent(final WPSReturnCode status)
    {
        final Intent intent = new Intent(LOCATION_UPDATE_INTENT, null);
        intent.putExtra(XPS_STATUS_EXTRA, status);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy()
    {
        // Make sure to abort any ongoing operations during onDestroy
        _xps.abort();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        throw new UnsupportedOperationException("use startService to start LocationService");
    }
}
