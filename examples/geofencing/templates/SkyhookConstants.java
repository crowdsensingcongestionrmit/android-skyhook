package com.skyhookwireless.samples.geofenceapisample;

abstract class SkyhookConstants
{
	private SkyhookConstants()
	{}

	static String getUsername()
	{
		return "@skyhook.username@";
	}

	static String getRealm()
	{
		return "@skyhook.realm@";
	}
}